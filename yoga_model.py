from flask import render_template,request,redirect,url_for,abort,send_from_directory,jsonify
from werkzeug.utils import secure_filename
from app import app,mongo
import imghdr
import cv2
import time
import argparse
import os
import platform
import sys
import time
import json
import numpy as np
import torch
from tqdm import tqdm
import natsort
from alphapose.utils.metrics import evaluate_mAP
from detector.apis import get_detector
from trackers.tracker_api import Tracker
from trackers.tracker_cfg import cfg as tcfg
from trackers import track
from alphapose.models import builder
from alphapose.utils.config import update_config
from alphapose.utils.detector import DetectionLoader
from alphapose.utils.transforms import flip, flip_heatmap,get_func_heatmap_to_coord
from alphapose.utils.vis import getTime
from alphapose.utils.webcam_detector import WebCamDetectionLoader
from alphapose.utils.writer import DataWriter
from alphapose.utils.pPose_nms import oks_pose_nms
from types import SimpleNamespace
from scipy.spatial import distance
from scipy import spatial
import shutil

# FLASK_APP=yoga_model.py FLASK_ENV=development flask run --host=0.0.0.0  --port=5000

global pose_model,args,cfg
args = SimpleNamespace(cfg='/home/palash/palash-projects/pose-esti/yoga_pose_estimation/configs/coco/resnet/256x192_res50_lr1e-3_1x.yaml', checkpoint='/home/palash/palash-projects/pose-esti/yoga_pose_estimation/pretrained_models/fast_res50_256x192.pth', debug=False, detbatch=5, detector='yolo', detfile='', device='cpu', eval=False, flip=False, format=None, gpus=[-1], inputimg='', inputlist='', inputpath='', min_box_area=0, pose_flow=False, pose_track=False, posebatch=80, profile=False, qsize=1024, save_img=False, save_video=False, showbox=False, sp=False, tracking=False, video='', vis=False, vis_fast=False, webcam=-1)
cfg = update_config(args.cfg)
pose_model = builder.build_sppe(cfg.MODEL, preset_cfg=cfg.DATA_PRESET)
print(f'Loading pose model from {args.checkpoint}...')
pose_model.load_state_dict(torch.load(args.checkpoint, map_location=args.device))
pose_model.to(args.device)
pose_model.eval()
print('pose model loaded')

torch.multiprocessing.set_start_method('forkserver', force=True)
torch.multiprocessing.set_sharing_strategy('file_system')

def check_input(inputpath):
    if len(inputpath):
        if len(inputpath) and inputpath != '/':
            for _,_, files in os.walk(inputpath):
                im_names = files
            im_names = natsort.natsorted(im_names)
        return 'image', im_names
    else:
        raise NotImplementedError

def remove_folder_content(root_dir_path):
    for root, dirs, files in os.walk(root_dir_path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))

def validate_image(stream):
    header = stream.read(512)
    stream.seek(0) 
    format = imghdr.what(None, header)
    if not format:
        return None
    return '.' + (format if format != 'jpeg' else 'jpg')

def remove_scores(input_list):
    for scores in input_list[2::3]:
        input_list.remove(scores)
    return input_list

def compare_two_arrays(user_key_points,db_key_points):
    
    user_array = np.array(user_key_points)
    db_array = np.array(db_key_points)

    cosineSimilarity = 1 - spatial.distance.cosine(db_array, user_array)
    cosine_result =  np.sqrt(2 * (1 - cosineSimilarity))
    eculine_dst = distance.euclidean(db_array,user_array)
    ling_np_dst = np.linalg.norm(db_array-user_array)
    print(f' From The image {"stand1_ideal"}"eculine_dst is =="{eculine_dst} ||| "ling_np_dst is =={ling_np_dst}" ||| "Distance is =="{cosine_result} ' )
    return cosine_result

def viz_key_points(input_user_dir,filename,save_folder,user_keypoints_list,db_key_points_list):

    l_pair = [
        (0, 1), (0, 2), (1, 3), (2, 4),  # Head
        (5, 18), (6, 18), (5, 7), (7, 9), (6, 8), (8, 10),# Body
        (17, 18), (18, 19), (19, 11), (19, 12),
        (11, 13), (12, 14), (13, 15), (14, 16),
        (20, 24), (21, 25), (23, 25), (22, 24), (15, 24), (16, 25),# Foot
        (26, 27),(27, 28),(28, 29),(29, 30),(30, 31),(31, 32),(32, 33),(33, 34),(34, 35),(35, 36),(36, 37),(37, 38),#Face
        (38, 39),(39, 40),(40, 41),(41, 42),(43, 44),(44, 45),(45, 46),(46, 47),(48, 49),(49, 50),(50, 51),(51, 52),#Face
        (53, 54),(54, 55),(55, 56),(57, 58),(58, 59),(59, 60),(60, 61),(62, 63),(63, 64),(64, 65),(65, 66),(66, 67),#Face
        (68, 69),(69, 70),(70, 71),(71, 72),(72, 73),(74, 75),(75, 76),(76, 77),(77, 78),(78, 79),(79, 80),(80, 81),#Face
        (81, 82),(82, 83),(83, 84),(84, 85),(85, 86),(86, 87),(87, 88),(88, 89),(89, 90),(90, 91),(91, 92),(92, 93),#Face
        (94,95),(95,96),(96,97),(97,98),(94,99),(99,100),(100,101),(101,102),(94,103),(103,104),(104,105),#LeftHand
        (105,106),(94,107),(107,108),(108,109),(109,110),(94,111),(111,112),(112,113),(113,114),#LeftHand
        (115,116),(116,117),(117,118),(118,119),(115,120),(120,121),(121,122),(122,123),(115,124),(124,125),#RightHand
        (125,126),(126,127),(115,128),(128,129),(129,130),(130,131),(115,132),(132,133),(133,134),(134,135)]#RightHand

    p_color = [(0, 255, 255), (0, 191, 255), (0, 255, 102), (0, 77, 255), (0, 255, 0),  # Nose, LEye, REye, LEar, REar
           (77, 255, 255), (77, 255, 204), (77, 204, 255), (191, 255, 77), (77, 191, 255), (191, 255, 77),  # LShoulder, RShoulder, LElbow, RElbow, LWrist, RWrist
           (204, 77, 255), (77, 255, 204), (191, 77, 255), (77, 255, 191), (127, 77, 255), (77, 255, 127),  # LHip, RHip, LKnee, Rknee, LAnkle, RAnkle, Neck
           (77, 255, 255), (0, 255, 255), (77, 204, 255),  # head, neck, shoulder
           (0, 255, 255), (0, 191, 255), (0, 255, 102), (0, 77, 255), (0, 255, 0), (77, 255, 255)] # foot

    line_color = [(0, 215, 255), (0, 255, 204), (0, 134, 255), (0, 255, 50),
                (0, 255, 102), (77, 255, 222), (77, 196, 255), (77, 135, 255), (191, 255, 77), (77, 255, 77),
                (77, 191, 255), (204, 77, 255), (77, 222, 255), (255, 156, 127),
                (0, 127, 255), (255, 127, 77), (0, 77, 255), (255, 77, 36), 
                (0, 77, 255), (0, 77, 255), (0, 77, 255), (0, 77, 255), (255, 156, 127), (255, 156, 127)]

    os.makedirs(save_folder,exist_ok=True)
    if os.path.exists(input_user_dir):
        img = cv2.imread(os.path.join(input_user_dir,filename))
    
    part_line = {}
    kp = np.array(user_keypoints_list)
    kp_x = kp[0::3]
    kp_y = kp[1::3]

    kp_scores = kp[2::3]
    # Draw keypoints
    for n in range(kp_scores.shape[0]):
        # if kp_scores[n] <= 0.6:
        #     continue
        cor_x, cor_y = int(kp_x[n]), int(kp_y[n])
        part_line[n] = (int(cor_x), int(cor_y))
        if n < len(p_color):
            cv2.circle(img, (int(cor_x), int(cor_y)), 2, p_color[n], -1)
        else:
            cv2.circle(img, (int(cor_x), int(cor_y)), 1, (255,255,255), 2)
    # Draw limbs
    for i, (start_p, end_p) in enumerate(l_pair):
        if start_p in part_line and end_p in part_line:
            start_xy = part_line[start_p]
            end_xy = part_line[end_p]
            if i < len(line_color):
                cv2.line(img, start_xy, end_xy, line_color[i], 2)
            else:
                cv2.line(img, start_xy, end_xy, (255,255,255), 1)

    cv2.imwrite(os.path.join(save_folder,filename),img)

@app.route('/',methods=['GET'])
def index():
    if request.method == 'GET':
        files = os.listdir(app.config['UPLOAD_PATH'])
        print("llll",files)
        return render_template('index.html',files=files)

@app.route('/user_re',methods=['POST','GET'])
def upload_files():

    if request.method == 'GET':
        return render_template('index.html')    
    elif request.method == 'POST':
        
        data = request.form.to_dict(flat=True)
        username = request.form['username']
        assan_name = request.form['assan_name']
        uploaded_file = request.files['image']

        filename = secure_filename(uploaded_file.filename)
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in app.config['UPLOAD_EXTENSIONS'] or \
                file_ext != validate_image(uploaded_file.stream):
                return "Invalid image", 400

        if username is not None:
            if os.path.exists(os.path.join(app.config['UPLOAD_PATH']+username,"input_img")):
                input_user_dir = os.path.join(app.config['UPLOAD_PATH']+username,"input_img")
                remove_folder_content(os.path.join(input_user_dir))
                uploaded_file.save(os.path.join(input_user_dir,filename))       
            if not mongo.db.usertable.find_one({"username":username}):
                mongo.db.usertable.insert_one(data)
                input_user_dir = os.path.join(app.config['UPLOAD_PATH']+username,"input_img")
                os.makedirs(input_user_dir,exist_ok=True)
                uploaded_file.save(os.path.join(input_user_dir,filename))
        else:
            return "Please enter different username"

        print('username',username)
        print('assan_name',assan_name)
        args.inputpath = input_user_dir
        mode, input_source = check_input(args.inputpath)
        det_loader = DetectionLoader(input_source, get_detector(args), cfg, args, batchSize=args.detbatch, mode=mode, queueSize=args.qsize)
        det_loader.start()

        data_len = det_loader.length
        im_names_desc = tqdm(range(data_len), dynamic_ncols=True)
        heatmap_to_coord = get_func_heatmap_to_coord(cfg)
        batchSize = args.posebatch
        keypoints_list = []
        try:
            for i in im_names_desc:
                with torch.no_grad():
                    (inps, orig_img, im_name, boxes, scores, ids, cropped_boxes) = det_loader.read()
                    # Pose Estimation
                    inps = inps.to(args.device)
                    datalen = inps.size(0)
                    leftover = 0
                    if (datalen) % batchSize:
                        leftover = 1
                    num_batches = datalen // batchSize + leftover
                    hm = []
                    for j in range(num_batches):
                        inps_j = inps[j * batchSize:min((j + 1) * batchSize, datalen)]
                        hm_j = pose_model(inps_j)
                        hm.append(hm_j)
                    hm = torch.cat(hm)
                    hm = hm.cpu()
                    data = dict()
                    for i in range(hm.shape[0]):
                        bbox = cropped_boxes[i].tolist()
                        pose_coords, pose_scores = heatmap_to_coord(hm[i], bbox)
                        keypoints = np.concatenate((pose_coords, pose_scores), axis=1)
                        keypoints = keypoints.reshape(-1).tolist()
                        keypoints_list.append(keypoints)
        except Exception as e:
            print(repr(e))
            print('An error as above occurs when processing the images, please check it')
            pass
        
        # user_key_points_list_nface = keypoints_list[0][15:len(keypoints_list[0])]
        # keypoints_list_new = remove_scores(user_key_points_list_nface)
        orig_key_points = keypoints_list[0].copy()
        user_keypoints_list = remove_scores(keypoints_list[0])

        from_db_key = mongo.db.yogaimgs.find_one({"image_id":assan_name})
        print("ooooooooooo",from_db_key)
        
        # db_key_points = [298.7550048828125, 93.50686645507812, 0.9217628836631775, 314.53997802734375, 82.98355102539062, 0.9578800797462463, 282.97003173828125, 82.98355102539062, 0.9622685313224792, 330.324951171875, 98.76852416992188, 0.9489538669586182, 256.6617431640625, 104.03018951416016, 0.9576358199119568, 367.1565856933594, 209.2633819580078, 0.8999477028846741, 219.83010864257812, 209.2633819580078, 0.8820187449455261, 372.4182434082031, 330.28155517578125, 0.8436256051063538, 225.09176635742188, 335.543212890625, 0.8528561592102051, 351.3716125488281, 424.9914245605469, 0.9632848501205444, 267.18505859375, 435.5147399902344, 0.821343183517456, 346.1099548339844, 388.1598205566406, 0.728617787361145, 240.8767547607422, 388.1598205566406, 0.7466286420822144, 525.00634765625, 388.1598205566406, 0.8642439842224121, 72.50364685058594, 414.4681091308594, 0.8108375072479248, 346.1099548339844, 456.5613708496094, 0.5957638621330261, 267.18505859375, 456.5613708496094, 0.5306324362754822]
        db_key_points_list = remove_scores(from_db_key['keypoints'])

        save_folder = os.path.join(app.config['UPLOAD_PATH']+username,"processed_img")
        remove_folder_content(os.path.join(save_folder))
        viz_key_points(input_user_dir,filename,save_folder,orig_key_points,db_key_points_list)

        print('save_folder+filename',save_folder)
        img_pro = cv2.imread(os.path.join(save_folder,filename))
        print(img_pro.shape)

        eculine_dst = compare_two_arrays(user_keypoints_list,db_key_points_list)
        return jsonify({"Distance":eculine_dst})
        # return redirect(url_for('index'))
        # return "Pala"
    
@app.route('/uploads/<filename>')
def upload(filename):
    return send_from_directory(app.config['UPLOAD_PATH'], filename)